//
//  WebServiceTests.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/11/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import XCTest
@testable import GitHubClient

class WebServiceTests: XCTestCase {
    
    func testExample() {
        
        let exp = expectation(description: "apiTest")
        
        API.requestRepositories(page: 1, completion: { rep in
            
            guard rep.count > 10 else {
                
                XCTFail()
                
                return
                
            }
            
            XCTAssertTrue(true)
            
            exp.fulfill()
            
        })
        
        self.waitForExpectations(timeout: 5, handler: { error in
            
            if error != nil {
        
                XCTFail()
                
            }
        
        })
        
        
    }
    
}

//
//  PullRequestTests.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/8/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import XCTest
@testable import GitHubClient

class PullRequestTests: XCTestCase {
    
    var pullRequest : PullRequest?
    
    override func setUp() {
        
        super.setUp()
        
        let user = ["login":"carlbrusell", "avatar_url":"lol"]
        
        let dic = ["title":"titulo", "body":"corpo", "user":user, "html_url":"url"] as [String : Any]
        
        pullRequest = PullRequest(dic: dic)
        
        
    }
    
    override func tearDown() {
        
        super.tearDown()
        
    }
    
    func testPullRequestInit() {
        
        XCTAssertEqual(pullRequest?.title, "titulo")
        XCTAssertEqual(pullRequest?.body, "corpo")
        XCTAssertEqual(pullRequest?.username, "carlbrusell")
        XCTAssertEqual(pullRequest?.avatarURL, "lol")
        XCTAssertEqual(pullRequest?.linkHTML, "url")
        
    }
    
    func testOppositeRepositoryInit(){
        
        XCTAssertNotEqual(pullRequest?.title, "tituloo")
        XCTAssertNotEqual(pullRequest?.body, "corpoo")
        XCTAssertNotEqual(pullRequest?.username, "carlbrusello")
        XCTAssertNotEqual(pullRequest?.avatarURL, "lolo")
        XCTAssertNotEqual(pullRequest?.linkHTML, "urlo")
        
    }

}

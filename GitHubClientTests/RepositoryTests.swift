//
//  GitHubClientTests.swift
//  GitHubClientTests
//
//  Created by Carl Osorio on 12/6/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import XCTest
@testable import GitHubClient

class RepositoryTests: XCTestCase {
    
    var repository : Repository?
    
    override func setUp() {
        
        super.setUp()
        
        let owner = ["avatar_url" : "lol", "login" : "lol"]
        
        let repositoryJson = ["name": "Toronto Pearson", "description": "Dublin", "forks_count": 1, "watchers_count": 24, "owner": owner] as [String : Any]
        
        repository = Repository(dic: repositoryJson)
        
        
    }
    
    override func tearDown() {
        
        super.tearDown()
        
    }
    
    func testRepositoryInit() {
        
        XCTAssertEqual(repository?.name, "Toronto Pearson")
        XCTAssertEqual(repository?.repDescription, "Dublin")
        XCTAssertEqual(repository?.forks, 1)
        XCTAssertEqual(repository?.stars, 24)
        XCTAssertEqual(repository?.avatarUrl, "lol")
        XCTAssertEqual(repository?.login, "lol")
        
    }
    
    func testOppositeRepositoryInit() {
        
        XCTAssertNotEqual(repository?.name, "Guarulhos")
        XCTAssertNotEqual(repository?.repDescription, "Sao Paulo")
        XCTAssertNotEqual(repository?.forks, 2)
        XCTAssertNotEqual(repository?.stars, 42)
        XCTAssertNotEqual(repository?.avatarUrl, "http://www.pudim.com.br")
        XCTAssertNotEqual(repository?.login, "@carlbrusell")
        
    }
    
    
}

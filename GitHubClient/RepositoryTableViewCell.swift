//
//  RepositoryTableViewCell.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/6/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var repoDescrp: UILabel!
    @IBOutlet weak var starsNumber: UILabel!
    @IBOutlet weak var userFullName: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var forkNumbers: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

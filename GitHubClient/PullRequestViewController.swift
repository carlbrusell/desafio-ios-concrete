//
//  ViewController.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/5/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import UIKit

class PullRequestViewController: UIViewController, UIWebViewDelegate {
    
    var pullRequest : PullRequest?
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = pullRequest?.title
        
        if let pullRequest = pullRequest {
        
            let url = URL(string: pullRequest.linkHTML)
            
            if let url = url {
            
                let urlRequest = URLRequest(url: url)
                
                webView.loadRequest(urlRequest)
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


//
//  PullRequest.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/6/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import UIKit

class PullRequest: NSObject {
    
    var title : String
    var body : String
    var username : String
    var avatarURL : String
    var linkHTML : String
    
    init(dic: [String : Any]){
        
        self.title = dic["title"] as! String
        self.body = dic["body"] as! String
        
        self.linkHTML = dic["html_url"] as! String
        
        let user = dic["user"] as! [String : Any]
        
        self.username = user["login"] as! String
        self.avatarURL = user["avatar_url"] as! String
        
        super.init()
        
    }

}

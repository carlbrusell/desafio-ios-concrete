//
//  AllRepositoriesTableViewController.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/5/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import UIKit
import SDWebImage

class AllRepositoriesTableViewController: UITableViewController {
    
    var repositories : [Repository] = []
    
    var collectedPages : [Int] = []
    
    var actualPage = 1
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.loadNewRepositories()
        
    }
    
    func loadNewRepositories(){
        
        /*
 
         Estou utilizando este array de páginas coletadas
         para evitar que a mesma info seja baixada duas vezes.
 
         */
        
        if !collectedPages.contains(actualPage) {
            
            collectedPages.append(actualPage)
        
            API.requestRepositories(page: actualPage, completion: { repositories in
                
                self.repositories.append(contentsOf: repositories)
                
                self.tableView.reloadData()
                
            })
            
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
        return repositories.count
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let rep = repositories[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryTableViewCell
        
        cell.name?.text = rep.name
        cell.repoDescrp?.text = rep.repDescription
        cell.username?.text = rep.login
        cell.avatarImage?.sd_setImage(with: URL(string: rep.avatarUrl))
        cell.forkNumbers?.text = "\(rep.forks)"
        cell.starsNumber?.text = "\(rep.stars)"
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let page = indexPath.row / 18
        
        if page + 1 != actualPage {
            
            actualPage = page + 1
            
            self.loadNewRepositories()
            
            print("loading new line \(page+1) - \(actualPage)")
            
        }
        
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let cell = sender as! UITableViewCell
        
        let indexPath = tableView.indexPath(for: cell)
        
        if let indexPath = indexPath {
        
            let rep = repositories[indexPath.row]

            let destination = segue.destination as! PullRequestsTableViewController

            destination.repository = rep
            
        }
        
    }
 

}

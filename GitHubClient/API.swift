//
//  API.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/5/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import UIKit
import Alamofire

class API: NSObject {
    
    static func requestRepositories(page: Int, completion: @escaping (_ repositories: [Repository]) -> Void) -> Void {
        
        let stringURL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        
        Alamofire.request(stringURL).responseJSON { response in
            
            if let JSON = response.result.value {
                
                var repositories : [Repository] = []
                
                let result = JSON as! [String : Any]
                
                let array = result["items"] as! [Any]
                
                for repository in array {
                    
                    let dic = repository as! [String : Any]
                    
                    let rep = Repository(dic: dic)
                    
                    repositories.append(rep)
                    
                }
                
                completion(repositories)
                
            }
            
        }
        
    }
    
    static func requestPullRequests(login: String, name: String, completion: @escaping (_ pullRequests: [PullRequest]) -> Void) {
    
        let url = "https://api.github.com/repos/\(login)/\(name)/pulls"
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            
            if let JSON = response.result.value {
                
                var pullRequests : [PullRequest] = []
                
                let result = JSON as! [Any]
                
                for res in result {
                    
                    let ress = res as! [String : Any]
                    
                    let pr = PullRequest(dic: ress)
                    
                    pullRequests.append(pr)
                    
                }
                
                completion(pullRequests)
                
            }
            
        })
        
    }

}

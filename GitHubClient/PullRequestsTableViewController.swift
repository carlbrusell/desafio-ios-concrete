//
//  PullRequestsTableViewController.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/6/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestsTableViewController: UITableViewController {
    
    var repository : Repository?
    var pullRequests : [PullRequest] = []

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        repository?.getPullRequests(completion: { pullRequests in
            
            self.pullRequests.append(contentsOf: pullRequests)
            self.tableView.reloadData()
        
        })
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pullRequests.count
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let pullrequest = pullRequests[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell", for: indexPath) as! PullRequestTableViewCell
        
        cell.title.text = pullrequest.title
        cell.username.text = pullrequest.username
        cell.body.text = pullrequest.body
        cell.avatar.sd_setImage(with: URL(string: pullrequest.avatarURL))

        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let cell = sender as! UITableViewCell
        
        let indexPath = tableView.indexPath(for: cell)
        
        if let indexPath = indexPath {
            
            let pr = pullRequests[indexPath.row]
        
            let destination = segue.destination as! PullRequestViewController
            
            destination.pullRequest = pr
            
        }
        
        
    }
    
}

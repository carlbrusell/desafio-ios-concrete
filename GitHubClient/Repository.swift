//
//  Repository.swift
//  GitHubClient
//
//  Created by Carl Osorio on 12/5/16.
//  Copyright © 2016 carlbrusell. All rights reserved.
//

import UIKit
import Alamofire

class Repository: NSObject {
    
    var name : String
    var repDescription : String
    var avatarUrl : String
    var login : String
    var forks : Int
    var stars : Int
    
    init(dic : [String : Any]) {
        
        name = dic["name"] as? String ?? ""
        
        repDescription = dic["description"] as? String ?? ""
        
        forks = dic["forks_count"] as? Int ?? 0
        stars = dic["watchers_count"] as? Int ?? 0
        
        let owner = dic["owner"] as? [String : Any]
        
        if let owner = owner {
        
            avatarUrl = owner["avatar_url"] as! String
            login = owner["login"] as! String
            
        }else{
            
            avatarUrl = ""
            login = ""
            
        }
        
        super.init()
        
    }
    
    func getPullRequests(completion: @escaping (_ pullRequests: [PullRequest]) -> Void) -> Void {
        
        API.requestPullRequests(login: login, name: name, completion: completion)
        
    }
    

}
